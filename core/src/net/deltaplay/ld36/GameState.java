package net.deltaplay.ld36;

public class GameState {
    public static final GameState instance = new GameState();

    private State state = State.RUNNING;
    public float hourglass  = 0;

    private GameState() {}

    public boolean success = true;

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public void reset() {
        this.state = State.RUNNING;
        this.hourglass = 0;
        this.success = true;
    }

    public enum State { RUNNING, FINISHED }
}
