package net.deltaplay.ld36;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.sun.media.jfxmediaimpl.MediaDisposer.Disposable;

public class Assets implements Disposable, AssetErrorListener {
    public static final String ATLAS = "data/normal.atlas";
    public static final String LEVEL_MAP = "data/level1.tmx";

    public static final Assets instance = new Assets();

    private AssetManager manager;

    public PlayerAssets playerAssets;
    public LevelAssets levelAssets;
    public HourglassAssets hourglassAssets;
    public LevelMapAssets levelMapAssets;
    public UIAssets uiAssets;

    private Assets() {}

    public void init() {
        this.manager = new AssetManager();

        manager.setErrorListener(this);
        manager.load(ATLAS, TextureAtlas.class);
        manager.finishLoading();

        TextureAtlas atlas = manager.get(ATLAS);
        playerAssets = new PlayerAssets(atlas);
        levelAssets = new LevelAssets(atlas);
        hourglassAssets = new HourglassAssets(atlas);
        uiAssets = new UIAssets(atlas);

        levelMapAssets = new LevelMapAssets();
    }

    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {

    }

    @Override
    public void dispose() {
        manager.dispose();
    }

    public class UIAssets {
        public final TextureRegion gameOver, levelComplete;

        public UIAssets(TextureAtlas atlas) {
            gameOver = atlas.findRegion("game-over");
            levelComplete = atlas.findRegion("level-complete");
        }
    }

    public class LevelMapAssets {
        public final TiledMap level;

        public LevelMapAssets() {
            level = new TmxMapLoader().load(LEVEL_MAP);
        }
    }

    public class HourglassAssets {
        public final TextureRegion outline;
        public final TextureRegion reflection;
        public final TextureRegion sand;
        public final TextureRegion fill;
        public final TextureRegion frame;

        public HourglassAssets(TextureAtlas atlas) {
            outline = atlas.findRegion("hourglass-outline");
            reflection = atlas.findRegion("hourglass-reflection");
            sand = atlas.findRegion("hourglass-sand");
            fill = atlas.findRegion("hourglass-fill");
            frame = atlas.findRegion("hourglass-frame");
        }
    }

    public class LevelAssets {
        public final AtlasRegion block;
        public final AtlasRegion ground;
        public final AtlasRegion target;
        public final AtlasRegion barrel;

        public LevelAssets(TextureAtlas atlas) {
            block = atlas.findRegion("block");
            ground = atlas.findRegion("ground");
            target = atlas.findRegion("target");
            barrel = atlas.findRegion("barrel");
        }
    }

    public class PlayerAssets {
        public final AtlasRegion standing;
        public final AtlasRegion pushing;
        public final AtlasRegion walking;

        public PlayerAssets(TextureAtlas atlas) {
            standing = atlas.findRegion("player-standing");
            pushing = atlas.findRegion("player-pushing");
            walking = atlas.findRegion("player-walking");
        }
    }
}
