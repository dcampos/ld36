package net.deltaplay.ld36.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalIteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import net.deltaplay.ld36.components.BarrelComponent;
import net.deltaplay.ld36.components.MovementComponent;
import net.deltaplay.ld36.components.MovementComponent.Direction;
import net.deltaplay.ld36.components.TransformComponent;

public class MovementSystem extends IntervalIteratingSystem {
    private Vector3 targetPos = new Vector3();

    private ComponentMapper<TransformComponent> tm = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<MovementComponent> mm = ComponentMapper.getFor(MovementComponent.class);
    private ComponentMapper<BarrelComponent> brm = ComponentMapper.getFor(BarrelComponent.class);

    public MovementSystem() {
        super(Family.all(MovementComponent.class, TransformComponent.class).get(), PlayerSystem.INTERVAL);
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
    }

    @Override
    protected void processEntity(Entity entity) {
        TransformComponent tc = tm.get(entity);
        MovementComponent mc = mm.get(entity);

        if (mc.direction != Direction.NONE) {

            if (entity.getComponent(BarrelComponent.class) != null)
                tc.rotation += getInterval() * 120;

            switch (mc.direction) {
                case UP:
                    tc.pos.y += getInterval() * mc.velocity.y;
                    if (tc.pos.y >= tc.targetPos.y) {
                        tc.pos.y = tc.targetPos.y;
                        mc.direction = Direction.NONE;
                    }
                    break;
                case DOWN:
                    tc.pos.y += getInterval() * -mc.velocity.y;
                    if (tc.pos.y <= tc.targetPos.y) {
                        tc.pos.y = tc.targetPos.y;
                        mc.direction = Direction.NONE;
                    }
                    break;
                case LEFT:
                    tc.pos.x += getInterval() * -mc.velocity.x;
                    if (tc.pos.x <= tc.targetPos.x) {
                        tc.pos.x = tc.targetPos.x;
                        mc.direction = Direction.NONE;
                    }
                    break;
                case RIGHT:
                    tc.pos.x += getInterval() * mc.velocity.x;
                    if (tc.pos.x >= tc.targetPos.x) {
                        tc.pos.x = tc.targetPos.x;
                        mc.direction = Direction.NONE;
                    }
                    break;
            }

        } else {
            tc.pos.x = MathUtils.round(tc.pos.x);
            tc.pos.y = MathUtils.round(tc.pos.y);
        }

    }

}
