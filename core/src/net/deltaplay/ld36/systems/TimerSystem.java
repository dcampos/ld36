package net.deltaplay.ld36.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import net.deltaplay.ld36.GameState;
import net.deltaplay.ld36.GameState.State;
import net.deltaplay.ld36.components.TimerComponent;

public class TimerSystem extends IteratingSystem {

    private static ComponentMapper<TimerComponent> tmapper = ComponentMapper.getFor(TimerComponent.class);

    public TimerSystem() {
        super(Family.all(TimerComponent.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        TimerComponent timer = tmapper.get(entity);

        timer.elapsed += deltaTime;

        GameState.instance.hourglass = timer.elapsed / timer.time;

        if (timer.elapsed >= timer.time) {
            timer.finished = true;
            GameState.instance.setState(State.FINISHED);
            GameState.instance.success = false;
        }
    }
}
