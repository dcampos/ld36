package net.deltaplay.ld36.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import net.deltaplay.ld36.components.TextureComponent;
import net.deltaplay.ld36.components.TransformComponent;

import java.util.Comparator;

public class RenderingSystem extends SortedIteratingSystem {

    private Viewport viewport;
    private SpriteBatch batch;
    private ShapeRenderer shapeRenderer;

    private static ComponentMapper<TransformComponent> transM = ComponentMapper.getFor(TransformComponent.class);
    private static ComponentMapper<TextureComponent> texM = ComponentMapper.getFor(TextureComponent.class);

    private Array<Entity> renderQueue;

    public RenderingSystem(Viewport viewport) {
        super(Family.all(TextureComponent.class, TransformComponent.class).get(), new ZComparator());

        this.renderQueue = new Array<Entity>();

        this.batch = new SpriteBatch();
        this.viewport = viewport;
        this.shapeRenderer = new ShapeRenderer();

    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        renderQueue.add(entity);
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glLineWidth(2.0f);

        batch.setProjectionMatrix(viewport.getCamera().combined);
        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
        shapeRenderer.setAutoShapeType(true);


        batch.begin();

        for (Entity entity : renderQueue) {

            TransformComponent transform = transM.get(entity);
            TextureComponent txc = texM.get(entity);
            TextureRegion texture = txc.texture;

            if (texture != null) {
                batch.draw(texture, transform.pos.x, transform.pos.y, 0.5f, 0.5f, 1f, 1f, 1f, 1f, transform.rotation);
            }

        }

        batch.end();

        shapeRenderer.begin(ShapeType.Line);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(0, 0, 10, 10);

        shapeRenderer.end();

        renderQueue.clear();
    }

    private static class ZComparator implements Comparator<Entity> {

        @Override
        public int compare(Entity e1, Entity e2) {
            return (int) (Math.signum(transM.get(e1).pos.z - transM.get(e2).pos.z));
        }
    }
}
