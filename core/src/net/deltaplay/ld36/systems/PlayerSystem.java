package net.deltaplay.ld36.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalIteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import net.deltaplay.ld36.Assets;
import net.deltaplay.ld36.components.MovementComponent;
import net.deltaplay.ld36.components.MovementComponent.Direction;
import net.deltaplay.ld36.components.PlayerComponent;
import net.deltaplay.ld36.components.PlayerComponent.Facing;
import net.deltaplay.ld36.components.TextureComponent;
import net.deltaplay.ld36.components.TransformComponent;

public class PlayerSystem extends IntervalIteratingSystem {

    public static final float INTERVAL = 1f / 30f;

    private ComponentMapper<TransformComponent> tm = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<MovementComponent> mm = ComponentMapper.getFor(MovementComponent.class);
    private ComponentMapper<TextureComponent> txm = ComponentMapper.getFor(TextureComponent.class);
    private ComponentMapper<PlayerComponent> pm = ComponentMapper.getFor(PlayerComponent.class);

    public PlayerSystem() {
        super(Family.all(PlayerComponent.class).get(), INTERVAL);
    }

    @Override
    protected void processEntity(Entity entity) {
        TransformComponent tc = tm.get(entity);
        MovementComponent mc = mm.get(entity);
        PlayerComponent pc = pm.get(entity);
        TextureComponent txc = txm.get(entity);

        if (mc.direction != Direction.NONE) {
            if (pc.pushing)
                txc.texture = Assets.instance.playerAssets.pushing;
            return;
        }


        if (Gdx.input.isKeyPressed(Keys.UP)) {
            pc.facing = Facing.UP;
            tc.rotation = 180;
            tc.targetPos.set(tc.pos.x, tc.pos.y + 1, 0);
            mc.setDirection(Direction.UP);
        } else if (Gdx.input.isKeyPressed(Keys.DOWN)) {
            pc.facing = Facing.DOWN;
            tc.rotation = 0;
            mc.setDirection(Direction.DOWN);
            tc.targetPos.set(tc.pos.x, tc.pos.y - 1, 0);
        } else if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
            pc.facing = Facing.RIGHT;
            tc.rotation = 90;
            mc.setDirection(Direction.RIGHT);
            tc.targetPos.set(tc.pos.x + 1, tc.pos.y, 0);
        } if (Gdx.input.isKeyPressed(Keys.LEFT)) {
            pc.facing = Facing.LEFT;
            tc.rotation = 270;
            tc.targetPos.set(tc.pos.x - 1, tc.pos.y, 0);
            mc.setDirection(Direction.LEFT);
        }

        if (mc.direction == Direction.NONE) {
            pc.pushing = false;
            txc.texture = Assets.instance.playerAssets.standing;
        }

    }
}
