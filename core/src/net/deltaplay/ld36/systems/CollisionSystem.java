package net.deltaplay.ld36.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalIteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import net.deltaplay.ld36.GameState;
import net.deltaplay.ld36.GameState.State;
import net.deltaplay.ld36.components.*;
import net.deltaplay.ld36.components.MovementComponent.Direction;

public class CollisionSystem extends IntervalIteratingSystem {
    public static final float LEVEL_WIDTH = 10f;
    public static final float LEVEL_HEIGHT = 10f;

    private ComponentMapper<TransformComponent> tm = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<MovementComponent> mm = ComponentMapper.getFor(MovementComponent.class);
    private ComponentMapper<BarrelComponent> brm = ComponentMapper.getFor(BarrelComponent.class);
    private ComponentMapper<PlayerComponent> pm = ComponentMapper.getFor(PlayerComponent.class);

    private ImmutableArray<Entity> blocks;
    private ImmutableArray<Entity> barrels;
    private ImmutableArray<Entity> targets;

    private int lockedBarrels = 0;

    public CollisionSystem() {
        super(Family.all(TransformComponent.class, MovementComponent.class).get(), PlayerSystem.INTERVAL);
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        blocks = engine.getEntitiesFor(Family.all(BlockComponent.class).get());
        barrels = engine.getEntitiesFor(Family.all(BarrelComponent.class).get());
        targets = engine.getEntitiesFor(Family.all(TargetComponent.class).get());
    }

    @Override
    protected void processEntity(Entity entity) {
        MovementComponent mc = mm.get(entity);
        TransformComponent tc = tm.get(entity);
        BarrelComponent bc = brm.get(entity);

        if (bc != null && bc.locked) {
            lockedBarrels++;

            if (lockedBarrels == barrels.size()) {
                GameState.instance.setState(State.FINISHED);
                GameState.instance.success = true;
            }
        }

        if (!canMove(entity)) {
            mc.setDirection(Direction.NONE);
            tc.targetPos.set(tc.pos);
        }
    }

    @Override
    protected void updateInterval() {
        lockedBarrels = 0;
        super.updateInterval();
    }

    private boolean canMove(Entity entity) {
        TransformComponent tc = tm.get(entity);
        MovementComponent mc = mm.get(entity);

        boolean insideX = (tc.targetPos.x >= 0 && tc.targetPos.x < LEVEL_WIDTH );
        boolean insideY = (tc.targetPos.y >= 0 && tc.targetPos.y < LEVEL_HEIGHT );

        if (!insideX || !insideY) {
            mc.setDirection(Direction.NONE);
            return false;
        }

        BarrelComponent bc = brm.get(entity);

        if (bc != null && bc.locked)
            return false;

        for (Entity block : blocks) {
            TransformComponent btc = tm.get(block);
            if (tc.targetPos.x == btc.pos.x && tc.targetPos.y == btc.pos.y)
                return false;
        }

        for (Entity target : targets) {
            BarrelComponent barrelComponent =
                ComponentMapper.getFor(BarrelComponent.class).get(entity);
            TransformComponent ttc = tm.get(target);
            MovementComponent tmc = mm.get(target);

            if (tc.pos.x == ttc.pos.x && tc.pos.y == ttc.pos.y
                && barrelComponent != null) {
                barrelComponent.setLocked(true);

                ImmutableArray<Entity> timers = getEngine().getEntitiesFor(Family.one(TimerComponent.class).get());
                if (timers.size() != 0) {
                    TimerComponent timer = timers.get(0).getComponent(TimerComponent.class);
                    timer.setTime(TimerComponent.DEF_TIME);
                }

                return false;
            }
        }

        for (Entity barrel : barrels) {
            if (barrel.equals(entity))
                continue;

            TransformComponent btc = tm.get(barrel);
            MovementComponent bmc = mm.get(barrel);
            BarrelComponent bbc = brm.get(barrel);
            PlayerComponent pc = pm.get(entity);

            if (tc.targetPos.x == btc.pos.x && tc.targetPos.y == btc.pos.y) {
                switch (mc.direction) {

                    case UP:
                        btc.targetPos.y = tc.targetPos.y + 1;
                        break;

                    case DOWN:
                        btc.targetPos.y = tc.targetPos.y - 1;
                        break;

                    case LEFT:
                        btc.targetPos.x = tc.targetPos.x - 1;
                        break;

                    case RIGHT:
                        btc.targetPos.x = tc.targetPos.x + 1;
                        break;
                }

                if (canMove(barrel)) {
                    bmc.setDirection(mc.direction);
                    if (pc != null) pc.pushing = true;
                    return true;
                } else {
                    return false;
                }
            }
        }

        return true;
    }
}
