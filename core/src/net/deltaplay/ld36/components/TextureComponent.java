package net.deltaplay.ld36.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TextureComponent implements Component {
    public TextureRegion texture;

    public TextureComponent setTexture(TextureRegion region) {
        this.texture = region;
        return this;
    }
}
