package net.deltaplay.ld36.components;

import com.badlogic.ashley.core.Component;

public class PlayerComponent implements Component {
    public boolean pushing = false;

    public enum Facing { UP, DOWN, LEFT, RIGHT }

    public Facing facing = Facing.RIGHT;

    public PlayerComponent setFacing(Facing facing) {
        this.facing = facing;
        return this;
    }

}
