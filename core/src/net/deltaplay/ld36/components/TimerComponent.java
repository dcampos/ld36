package net.deltaplay.ld36.components;

import com.badlogic.ashley.core.Component;

public class TimerComponent implements Component {
    public static final float DEF_TIME = 40f;

    public float time = DEF_TIME, elapsed;
    public boolean finished;

    public TimerComponent setTime(float time) {
        this.time = time;
        this.elapsed = 0;
        return this;
    }
}
