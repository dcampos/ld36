package net.deltaplay.ld36.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class MovementComponent implements Component {
    public Vector3 velocity = new Vector3();
    public Direction direction = Direction.NONE;

    public Vector2 accel = new Vector2();

    public MovementComponent setVelocity(float x, float y) {
        this.velocity.set(x, y, 0);
        return this;
    }

    public MovementComponent setDirection(Direction direction) {
        this.direction = direction;
        return this;
    }

    public enum Direction {
        UP, DOWN, LEFT, RIGHT, NONE
    }

}
