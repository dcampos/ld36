package net.deltaplay.ld36.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector3;

public class TransformComponent implements Component {
    public Vector3 pos = new Vector3();
    public Vector3 targetPos = new Vector3();
    public float rotation = 0.0f;

    public TransformComponent setPosition(float x, float y, float z) {
        this.pos.set(x, y, z);
        this.targetPos.set(pos);
        return this;
    }
}
