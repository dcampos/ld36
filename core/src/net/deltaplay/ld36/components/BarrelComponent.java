package net.deltaplay.ld36.components;

import com.badlogic.ashley.core.Component;

public class BarrelComponent implements Component {
    public boolean locked = false;

    public BarrelComponent setLocked(boolean locked) {
        this.locked = locked;
        return this;
    }
}
