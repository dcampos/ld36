package net.deltaplay.ld36.util;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import net.deltaplay.ld36.Assets;
import net.deltaplay.ld36.components.*;
import net.deltaplay.ld36.components.PlayerComponent.Facing;

import java.util.Iterator;

public class LevelLoader {
    public static final float TILE_SIZE = 64f;
    public static final int SPEED = 3;

    public static void load(TiledMap map, PooledEngine engine) {
        TiledMapTileLayer groundLayer = (TiledMapTileLayer) map.getLayers().get("ground");
        TiledMapTileLayer blocksLayer = (TiledMapTileLayer) map.getLayers().get("blocks");
        MapLayer typesLayer = map.getLayers().get("types");

        for (int i = 0; i < groundLayer.getWidth(); i++) {
            for (int j = 0; j < groundLayer.getHeight(); j++) {
                MapProperties props = groundLayer.getCell(i, j).getTile()
                        .getProperties();

                Iterator<String> keys = props.getKeys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    String value = (String) props.get(key);

                    if ("ground".equals(value)) {
                        // create ground component
                        Entity ground = engine.createEntity();
                        ground.add(new TextureComponent().setTexture(Assets.instance.levelAssets.ground));
                        ground.add(new TransformComponent().setPosition(i, j, 0));
                        engine.addEntity(ground);
                    }
                }
            }
        }

        for (int i = 0; i < blocksLayer.getWidth(); i++) {
            for (int j = 0; j < blocksLayer.getHeight(); j++) {
                if (blocksLayer.getCell(i, j) == null)
                    continue;

                MapProperties props = blocksLayer.getCell(i, j).getTile()
                        .getProperties();

                Iterator<String> keys = props.getKeys();

                while (keys.hasNext()) {
                    String key = keys.next();
                    String value = (String) props.get(key);

                    if ("block".equals(value)) {
                        // create block component
                        Entity block = engine.createEntity();
                        block.add(new TextureComponent().setTexture(Assets.instance.levelAssets.block));
                        block.add(new TransformComponent().setPosition(i, j, 1));
                        block.add(new BlockComponent());
                        engine.addEntity(block);
                    } else if ("target".equals(value)) {
                        // create target component
                        Entity target = engine.createEntity();
                        target.add(new TextureComponent().setTexture(Assets.instance.levelAssets.target));
                        target.add(new TransformComponent().setPosition(i, j, 1));
                        target.add(new TargetComponent());
                        engine.addEntity(target);
                    } else if ("barrel".equals(value)) {
                        // create barrel component
                        Entity barrel = engine.createEntity();
                        barrel.add(new TextureComponent().setTexture(Assets.instance.levelAssets.barrel));
                        barrel.add(new TransformComponent().setPosition(i, j, 2));
                        barrel.add(new MovementComponent().setVelocity(SPEED, SPEED));
                        barrel.add(new BarrelComponent());
                        engine.addEntity(barrel);
                    }

                }
            }
        }

        for (MapObject object : typesLayer.getObjects()) {
            if ("player".equals(object.getProperties().get("type"))) {
                float x = Float.valueOf(object.getProperties().get("x").toString()) / TILE_SIZE;
                float y = Float.valueOf(object.getProperties().get("y").toString()) / TILE_SIZE;
                Entity player = engine.createEntity();
                player.add(new PlayerComponent().setFacing(Facing.RIGHT));
                player.add(new TransformComponent().setPosition(x, y, 2));
                player.add(new TextureComponent().setTexture(Assets.instance.playerAssets.standing));
                player.add(new MovementComponent().setVelocity(SPEED, SPEED));
                engine.addEntity(player);
            }
        }

    }
}
