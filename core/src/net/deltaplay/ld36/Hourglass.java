package net.deltaplay.ld36;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class Hourglass extends Image {
    TextureRegion frame, outline, fill, reflection, sand;

    float percent = 0.0f;

    public Hourglass() {
        frame = Assets.instance.hourglassAssets.frame;
        outline = Assets.instance.hourglassAssets.outline;
        fill = Assets.instance.hourglassAssets.fill;
        sand = Assets.instance.hourglassAssets.sand;
        reflection = Assets.instance.hourglassAssets.reflection;
    }

    public void setPercent(float percent) {
        this.percent = percent > 1 ? this.percent : percent;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(Assets.instance.hourglassAssets.frame, getX(), getY());
        batch.draw(fill, getX() + getWidth() * .5f - fill.getRegionWidth() * .5f,
                getY() + getHeight() * .5f - fill.getRegionHeight() * .5f );

        int offset = (int)(sand.getRegionHeight() * percent / 2);

        batch.draw(sand.getTexture(), getX() + getWidth() * .5f - sand.getRegionWidth() * .5f,
                getY() + getHeight() * .5f - sand.getRegionHeight() * .5f,
                sand.getRegionX(),
                sand.getRegionY() + offset,
                sand.getRegionWidth(),
                sand.getRegionHeight() - offset);

        batch.draw(sand.getTexture(),
                getX() + getWidth() * .5f - sand.getRegionWidth() * .5f, // x
                getY() + getHeight() * .5f - sand.getRegionHeight() * .5f, // y
                sand.getRegionWidth(),
                offset,
                sand.getRegionX(), // srcX
                sand.getRegionY(), // srcY
                sand.getRegionWidth(), // srchW
                offset,
                false,
                true
        );

        batch.draw(reflection, getX() + getWidth() * .5f - reflection.getRegionWidth() * .5f,
                getY() + getHeight() * .5f - reflection.getRegionHeight() * .5f );
        batch.draw(outline, getX() + getWidth() * .5f - outline.getRegionWidth() * .5f,
                getY() + getHeight() * .5f - outline.getRegionHeight() * .5f );
    }
}
