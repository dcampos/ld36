package net.deltaplay.ld36;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.FPSLogger;
import net.deltaplay.ld36.screens.GameScreen;

public class LD36 extends Game {
    FPSLogger fps;

    @Override
    public void create() {
        Assets.instance.init();
        fps = new FPSLogger();
        setScreen(new GameScreen(this));
    }

    @Override
    public void render() {
        super.render();
        fps.log();
    }

    @Override
    public void dispose() {
        Assets.instance.dispose();
    }
}
