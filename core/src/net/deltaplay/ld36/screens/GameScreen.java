package net.deltaplay.ld36.screens;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import net.deltaplay.ld36.Assets;
import net.deltaplay.ld36.GameState;
import net.deltaplay.ld36.GameState.State;
import net.deltaplay.ld36.Hourglass;
import net.deltaplay.ld36.LD36;
import net.deltaplay.ld36.components.TimerComponent;
import net.deltaplay.ld36.systems.*;
import net.deltaplay.ld36.util.LevelLoader;

public class GameScreen extends ScreenAdapter {
    LD36 game;
    PooledEngine engine;
    FitViewport viewport;

    private Stage stage;
    private final Hourglass hourglass;
    private Image gameOverImage;
    private Image levelCompleteImage;
    private Label restartLabel;
    private Image overlay;

    public GameScreen(LD36 game) {
        this.game = game;
        this.engine = new PooledEngine();

        float viewportWidth = 12;
        float viewportheight = 10;

        this.viewport = new FitViewport(Gdx.graphics.getWidth() / 64f, viewportheight);
//        this.viewport = new FitViewport(viewportWidth, viewportheight);

        Gdx.input.setInputProcessor(new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                if (keycode == Keys.SPACE) {
                    startGame();
                }
                return true;
            }
        });

        this.stage = new Stage(new ScreenViewport());

        hourglass = new Hourglass();
        hourglass.setWidth(132);
        hourglass.setHeight(132);
        hourglass.setPosition(stage.getWidth() - 146, stage.getHeight() - 142);
        stage.addActor(hourglass);

    }

    @Override
    public void show() {
        super.show();

        Pixmap pixmap = new Pixmap((int)stage.getWidth(), (int)stage.getHeight(), Format.RGBA4444);
        pixmap.setColor(new Color(0x000000AA));
        pixmap.fill();

        Texture texture = new Texture(pixmap);

        overlay = new Image(texture);
        stage.addActor(overlay);
        overlay.setVisible(false);

        gameOverImage = new Image(Assets.instance.uiAssets.gameOver);
        gameOverImage.setVisible(false);
        gameOverImage.setPosition(stage.getWidth() * .5f - gameOverImage.getWidth() * .5f,
                stage.getHeight() * .5f + gameOverImage.getHeight());
        levelCompleteImage = new Image(Assets.instance.uiAssets.levelComplete);
        levelCompleteImage.setVisible(false);
        levelCompleteImage.setPosition(stage.getWidth() * .5f - levelCompleteImage.getWidth() * .5f,
                stage.getHeight() * .5f + levelCompleteImage.getHeight());

        stage.addActor(gameOverImage);
        stage.addActor(levelCompleteImage);

        Skin skin = new Skin(Gdx.files.internal("uiskin.json"));
        restartLabel = new Label("Press SPACE to restart", skin);
        restartLabel.setVisible(false);
        restartLabel.setPosition(stage.getWidth() * .5f - restartLabel.getWidth() * .5f,
                stage.getHeight() * .5f - 100);
        restartLabel.addAction(Actions.forever(Actions.sequence(Actions.fadeOut(.5f), Actions.fadeIn(.5f))));
        stage.addActor(restartLabel);

        startGame();
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        Gdx.gl.glClearColor(233f / 255f, 198f / 255f, 175f / 255f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        engine.update(delta);

        hourglass.setPercent(GameState.instance.hourglass);

        stage.act();

        stage.draw();

        if (GameState.instance.getState() == State.FINISHED) {
            pauseGame();
        }
    }

    private void startGame() {
        engine.removeAllEntities();

        restartLabel.setVisible(false);
        gameOverImage.setVisible(false);
        levelCompleteImage.setVisible(false);
        overlay.setVisible(false);

        GameState.instance.reset();

        engine.addSystem(new RenderingSystem(viewport));
        engine.addSystem(new PlayerSystem());
        engine.addSystem(new CollisionSystem());
        engine.addSystem(new MovementSystem());
        engine.addSystem(new TimerSystem());

        Entity timerEntity = engine.createEntity();
        timerEntity.add(new TimerComponent());
        engine.addEntity(timerEntity);

        LevelLoader.load(Assets.instance.levelMapAssets.level, engine);

    }

    private void pauseGame() {
        engine.getSystem(PlayerSystem.class).setProcessing(false);
        engine.getSystem(MovementSystem.class).setProcessing(false);
        engine.getSystem(TimerSystem.class).setProcessing(false);
        engine.getSystem(CollisionSystem.class).setProcessing(false);

        overlay.setVisible(true);
        restartLabel.setVisible(true);

        if (GameState.instance.success) {
            levelCompleteImage.setVisible(true);
        } else {
            gameOverImage.setVisible(true);
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);


        // Reset HUD...
    }
}
